package functionality

import (
	"fmt"
)

func QueryCurrencyResponse(usrRequest Rates) (string, error){

	Exchange := 0.0
	err := error(nil)

	if usrRequest.BaseCurrency == "EUR" {
		Exchange, err = GetEURExchange(usrRequest.TargetCurrency)
		if err != nil {
			return "Sorry, could not find data.", err
		}
	}else{
		Exchange, err = GetExchange(usrRequest)
		if err != nil {
			return "Sorry, could not find data.", err
		}
	}
	// TODO: Round the number
	// UNTESTED
	exchangeValue := float64(int64(2000*(Exchange+0.00005)))/2000
	a := fmt.Sprint(exchangeValue)

	return "The exchange rate between " + usrRequest.BaseCurrency + " and " + usrRequest.TargetCurrency + " is " + a, nil
}
