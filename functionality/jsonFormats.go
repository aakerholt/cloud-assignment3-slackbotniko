package functionality

import "gopkg.in/mgo.v2/bson"


// TODO: Split this to two files
type CurrencyLoad struct {
	Id              bson.ObjectId	`bson:"_id,omitempty"`
	WebHookURL      string       	`json:"webhookURL"`
	BaseCurrency    string       	`json:"baseCurrency"`
	TargetCurrency  string       	`json:"targetCurrency"`
	MinTriggerValue float64      	`json:"minTriggerValue"`
	MaxTriggerValue float64       	`json:"maxTriggerValue"`
}

type Rates struct {
	BaseCurrency   	string 			`json:"baseCurrency"`
	TargetCurrency 	string 			`json:"targetCurrency"`
}

type ChatBot struct {
	Id              bson.ObjectId	`bson:"_id,omitempty"`
	WebHookURL		string			`json:"webhookURL"`
}

type RawFixer struct {
	Base      		string          `json:"base"`
	Date      		string          `json:"date"`
	LocalRate 	map[string]float64 	`json:"rates"`
}


// 				Dialog flow data

// POST

type ResponseData struct{
	Lang 			string			`json:"lang"`
	Query 			string			`json:"query"`
	SessionID		string			`json:"sessionId"`
}


// GET
type DialogFlowData struct{
	ResultData		Result			`json:"result"`
	Status			DialogStatus	`json:"status"`
	SessionID		string			`json:"sessionId"`
}

type DialogStatus struct{
	Code			int				`json:"code"`
	ErrorType		string			`json:"errorType"`
}

type Result struct{
	FulfillmentStruct Fulfillment	`json:"fulfillment"`
}

type Fulfillment struct{
	Messages	[]Payload			`json:"messages"`
}

type Payload struct {
	PayloadData		Rates			`json:"payload"`
}
//-------------------------------------------------


/*type DialogFlowLoad struct {
	baseCurrency
}*/

/*

{
  "baseCurrency": "$currency1",
  "targetCurrency": "$currency2"
}

{
"webhookURL":"https://discordapp.com/api/webhooks/375013560788451331/4NO43EJrdT0pg-BL_RgPXgOuHYpiBvxFMljSxY6BwJapDhzO0wsADvPkcpEarD-LP5kB",
"baseCurrency":"EUR",
"targetCurrency":"NOK",
"minTriggerValue":2.3,
"maxTriggerValue":5.6
}
*/
