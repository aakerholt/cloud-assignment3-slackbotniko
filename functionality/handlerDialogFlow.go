package functionality

import (
	"encoding/json"
	"net/http"
	"time"
	"log"
	"bytes"
)

/*

func request(w http.ResponseWriter, r *http.Request){

	if r.Method == "POST"{

		fulfillment, err := GetDialogFlowPayload(r)
		if err != nil{
			http.Error(w, "Could not retrieve post data in form DialogFlow", http.StatusInternalServerError)
			return
		}


	}

}
*/

func DialogFlowHandler(w http.ResponseWriter, r *http.Request){

	if r.Method == "POST" {

		r.Header.Set("Content-Type", "application/json")

		dialogData := DialogFlowData{}

		err := json.NewDecoder(r.Body).Decode(&dialogData)
		if err != nil {
			return
		}

		defer r.Body.Close()

		myClient := http.Client{
			Timeout: time.Second * 10,
		}

		queryURL := "https://api.dialogflow.com/v1/query?v=20170712"
		// lag body
		response := ResponseData{}
		response.Lang = "en"
		response.Query, err = QueryCurrencyResponse(dialogData.ResultData.FulfillmentStruct.Messages[0].PayloadData)
		if err != nil{
			log.Print("bot failed to create currency response")
		}
		response.SessionID = dialogData.SessionID

		body, err := json.Marshal(response)
		if err != nil{
			log.Print("failed to marshal response")
		}

		url
		resp, err := http.PostForm(queryURL, )

		/*req, err := http.NewRequest("POST", queryURL, ioBody)
		// ...
		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization", "Bearer 8544cbec217f412d8cf31e3da5252878")
		_, err = myClient.Do(req)
		if err != nil{
			log.Print(err.Error())
		}else {
			log.Printf("%d", response)
		}*/
	}
}


func GetFlowJSON() Rates{


	flowTestData := []byte(`{
  "id": "aae70154-3955-4cd2-9d6d-1f86e103f651",
  "timestamp": "2017-11-10T21:27:26.479Z",
  "lang": "en",
  "result": {
    "source": "agent",
    "resolvedQuery": "What is the current exchange rate between Norwegian Kroner and Euro?",
    "action": "",
    "actionIncomplete": false,
    "parameters": {
      "currency1": "NOK",
      "currency2": "EUR"
    },
    "contexts": [
      {
        "name": "whatisthecurrentexchangeratebetweennorwegiankronerandeuro-followup",
        "parameters": {
          "currency1": "NOK",
          "currency2": "EUR",
          "currency1.original": "Norwegian Kroner",
          "currency2.original": "Euro"
        },
        "lifespan": 2
      }
    ],
    "metadata": {
      "intentId": "a6c086ae-0821-47d3-9ada-c131ea11500a",
      "webhookUsed": "false",
      "webhookForSlotFillingUsed": "false",
      "intentName": "getExchange"
    },
    "fulfillment": {
      "speech": "",
      "messages": [
        {
          "type": 4,
          "payload": {
            "baseCurrency": "NOK",
            "targetCurrency": "EUR"
          }
        }
      ]
    },
    "score": 1
  },
  "status": {
    "code": 200,
    "errorType": "success"
  },
  "sessionId": "821dd329-6711-4da4-9fee-8a3a806fc950"
}`)

	flowData := DialogFlowData{}

	json.Unmarshal(flowTestData, &flowData)

	return flowData.ResultData.FulfillmentStruct.Messages[0].PayloadData
}
