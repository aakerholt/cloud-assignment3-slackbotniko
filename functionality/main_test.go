package functionality

// Sources:
// http://cs-guy.com/blog/2015/01/test-main/

import (
	"testing"
	"os"
	"bytes"
	"encoding/json"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var TestBody []byte
var TestFindBody []byte
var TestIo *bytes.Reader
var TestFindIo *bytes.Reader
var TestObjectId string


func TestMain(m *testing.M){
	MongoDB = GetADB()
	MongoDB.Init()
	Setup()
	code := m.Run()
	os.Exit(code)
}

func Setup(){
	TestBody = []byte(`{"webhookURL":"deleteMe","baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":8,"maxTriggerValue":10}`)
	TestFindBody = []byte(`{"baseCurrency":"EUR","targetCurrency":"NOK"}`)
	TestIo = bytes.NewReader(TestBody)
	TestFindIo = bytes.NewReader(TestFindBody)

	testHook := CurrencyLoad{}

	json.Unmarshal(TestBody, &testHook)

	session, err := mgo.Dial(MongoDB.MongoURL)
	if err != nil {
		panic("Failed to connect to db")
	}
	defer session.Close()

	err = session.DB(MongoDB.DatabaseName).C(MongoDB.CurrencyHookCollection).Insert(testHook)
	if err != nil {
		panic("failed to insert test object")
	}
	testHookRes, got := MongoDB.Get(testHook.WebHookURL)
	if got == false {
		panic("Could not retrieve test object from db")
	}
	TestObjectId = "/" + bson.ObjectId(testHookRes.Id).Hex()
}

