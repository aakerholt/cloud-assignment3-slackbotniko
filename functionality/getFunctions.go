package functionality

import (
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"io/ioutil"
	"net/http"
	"errors"
)

// Sources:
// Removing decimals:
// https://stackoverflow.com/questions/18390266/how-can-we-truncate-float64-type-to-a-particular-precision-in-golang

/*
"mongodb://Avokado:Verify123@ds243805.mlab.com:43805/currencydb"
"currencydb"

"mongodb://localhost",
		"CurrencyDB"
*/
func GetADB() *MongoDBInfo {
	db := MongoDBInfo{
		"mongodb://Avokado:Verify123@ds243805.mlab.com:43805/currencydb",
		"currencydb",
		"hookCollection",
		"ratesCollection",
	}

	_, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}

	return &db
}

// GetBody ...
// @Return: Body and error/nil
func GetBody(url string, myClient *http.Client) ([]byte, error) {

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		body := []byte(nil)
		return body, err
	}

	// Apply header to request
	req.Header.Set("User-Agent", "AkselHj")

	// Try to execute request
	res, doError := myClient.Do(req)
	if doError != nil {
		body := []byte(nil)
		return body, doError
	}

	defer res.Body.Close()

	body, readError := ioutil.ReadAll(res.Body)

	if readError != nil {
		return body, readError
	}

	return body, nil
}

func getCurrencyLoad(r *http.Request) (CurrencyLoad, error) {
	currencyLoad := CurrencyLoad{}

	err := json.NewDecoder(r.Body).Decode(&currencyLoad)
	if err != nil {
		return currencyLoad, err
	}

	defer r.Body.Close()
	return currencyLoad, err
}

func GetDialogFlowPayload(r *http.Request)(Rates, error){
	fulfillment := DialogFlowData{}

	err := json.NewDecoder(r.Body).Decode(&fulfillment)
	if err != nil {
		return Rates{}, err
	}

	defer r.Body.Close()

	return fulfillment.ResultData.FulfillmentStruct.Messages[0].PayloadData, nil
}

func GetHookContent(hook CurrencyLoad, rawFixer RawFixer) string {

	a := fmt.Sprint(rawFixer.LocalRate[hook.TargetCurrency])
	b := fmt.Sprint(hook.MinTriggerValue)
	c := fmt.Sprint(hook.MaxTriggerValue)

	hookContent := "baseCurrency: " + hook.BaseCurrency + "\ntargetCurrency: " + hook.TargetCurrency +
		"\ncurrentRate: " + a +
		"\nminTriggerValue: " + b + "\nmaxTriggerValue: " + c

	return hookContent
}

func GetEURExchange(target string)(float64, error){
	localData, got := MongoDB.GetLocalFixer(0)
	if got == false{
		return 0, errors.New("DB failed at GetLocalFixer()")
	}
	rates := RawFixer{}

	byteData, err := json.Marshal(localData)
	if err != nil{
		return 0, err
	}
	err = json.Unmarshal(byteData, &rates)
	if err != nil{
		return 0, err
	}
	return rates.LocalRate[target], nil
}

func GetExchange(rates Rates)(float64, error){

	rawFixer := RawFixer{}
	exchangeValue := 0.0

	localFixer, got := MongoDB.GetLocalFixer(0)
	if got == false{
		return 0, errors.New("could not retrieve rates in db")
	}

	fixerData, err := json.Marshal(localFixer)
	if err != nil{
		return 0, err
	}

	err = json.Unmarshal(fixerData, &rawFixer)
	if err != nil{
		return 0, err
	}

	if rawFixer.LocalRate[rates.BaseCurrency] != 0 {
		if rates.TargetCurrency != "EUR" {
			exchangeValue = rawFixer.LocalRate[rates.TargetCurrency] / rawFixer.LocalRate[rates.BaseCurrency]
		} else {
			exchangeValue = 1.0 / rawFixer.LocalRate[rates.BaseCurrency]
		}
	}

	return exchangeValue, nil
}

func GetRates(myClient *http.Client, base string) (interface{}, error) {

	var fixerInterface interface{}

	fixerURL := "http://api.fixer.io/latest?base=" + base

	fixerBody, err := GetBody(fixerURL, myClient)
	if err != nil {
		return fixerInterface, err
	}

	err = json.Unmarshal(fixerBody, &fixerInterface)
	if err != nil {
		return fixerInterface, err
	}

	return fixerInterface, err

}
