package functionality

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"testing"
)

func TestMongoDBInfo_Add(t *testing.T) {

	testHook := CurrencyLoad{}

	json.Unmarshal(TestBody, &testHook)

	session, err := mgo.Dial(MongoDB.MongoURL)
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	err = session.DB(MongoDB.DatabaseName).C(MongoDB.CurrencyHookCollection).Insert(testHook)
	if err != nil {
		t.Error("Failed to insert test data")
	}

	testHookRes, got := MongoDB.Get(testHook.WebHookURL)
	if got == false {
		t.Error("Could not retrieve added data")
	}
	if testHookRes.WebHookURL != testHook.WebHookURL || testHookRes.MinTriggerValue != testHook.MinTriggerValue {
		t.Error("Failed to add correct data")
	}
}

func TestMongoDBInfo_Get(t *testing.T) {

	testHook := CurrencyLoad{}

	json.Unmarshal(TestBody, &testHook)

	testHookRes, got := MongoDB.Get(testHook.WebHookURL)
	if got == false {
		t.Error("Could not retrieve added data")
	}
	if testHookRes.WebHookURL != testHook.WebHookURL || testHookRes.MinTriggerValue != testHook.MinTriggerValue {
		t.Error("Failed to add correct data")
	}

}

/*
func TestMongoDBInfo_DeleteByURL(t *testing.T) {
	MongoDB.DeleteByURL("deleteMe")
	_, got := MongoDB.Get("deleteMe")
	if got == true {
		t.Error("Failed to remove test object!")
	}
}
*/
