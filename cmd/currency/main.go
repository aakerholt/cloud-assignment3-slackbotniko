// Author: Aksel Hjerpbakk
// Studentnr: 997816

package main

import (
	"cloud-assignment2-currencyhook/functionality"
	"net/http"
	"os"
)


// TODO: Json struct for dialog flow payload
// TODO: implement with db
// TODO: Softcode base?
// TODO: Compute response

func main() {
	functionality.MongoDB = functionality.GetADB()
	functionality.MongoDB.Init()
//	functionality.MongoDB.AddDailyFix()
	port := os.Getenv("PORT")
	http.HandleFunc("/", functionality.URLHandler)
	http.ListenAndServe(":"+port, nil)
}
