package main

import (
	"cloud-assignment2-currencyhook/functionality"
)

func main() {
	functionality.MongoDB = functionality.GetADB()
	functionality.MongoDB.Init()
	functionality.MongoDB.DailyCheckHooks()
	functionality.DailyRoutine()
}
